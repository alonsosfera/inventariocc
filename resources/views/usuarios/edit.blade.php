@extends('layouts.app')

@section('titulo')
  <title>Usuarios Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Usuario
		</div>

		<div class="card-body">

  		<form action="/usuarios/{{$usuario->id}}" method="post">
  			{{method_field('PATCH')}}
  			{{ csrf_field() }}

        <div class="form-group">
  				<label for="username">Nombre de Usuario</label>
          <div class="col">
  					<input type="text" class="form-control @error ('username') is-invalid  @enderror" name="username" value="{{$usuario->username}}" required autofocus>
            @if ($errors->has('username'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('username') }}</strong>
  							</span>
            @endif
          </div>
        </div>

  			<div class="form-group">
  				<label for="cargo">Tipo de Cargo</label>
          <div class="col">
            <select class="form-control @error ('cargo') is-invalid  @enderror" name="cargo" required>
              <option value="{{$usuario->tipo_cargo}}" disabled selected>{{$usuario->tipo_cargo}}</option>
              <option value="Mantenimiento">Mantenimiento</option>
              <option value="Administrador"> Administrador</option>
            </select>
            @if ($errors->has('cargo'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('cargo') }}</strong>
  							</span>
            @endif
          </div>
        </div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
