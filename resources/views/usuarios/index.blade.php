@extends('layouts.app')

@section('titulo')
  <title>Usuarios</title>
@endsection
@section('content')

  @if(session()->get('mensaje'))
    <p id="EditMessage" hidden>
  		{{session()->get('mensaje')}}
  	</p>
  @endif
  <div class="container">
  	<h1>Usuarios</h1>

  	<hr/>
    <div id="toolbar">
      <p id="ActiveRoute" hidden>/usuarios/</p>
      <a href="{{ route('register') }}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
      <button id="BtnEditar" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
      <button id="BtnPsw" title="Restablecer contraseña" class="btn btn-info"><i class="fa fa-unlock-alt"></i></button>
      <button id="BtnEliminar" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </div>
    <table
      id="tableInv"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-refresh="true"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-toolbar="#toolbar"
      data-page-list="[10, 25, 50, 100, All]"
      data-single-select="true"
      data-click-to-select="true"
      data-url="/listu"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="username" data-sortable="true">Nombre de Usuario</th>
          <th data-field="tipo_cargo" data-sortable="true">Tipo</th>
        </tr>
      </thead>

    </table>
    </div>
  </div>
@endsection
