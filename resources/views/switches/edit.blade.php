@extends('layouts.app')

@section('titulo')
  <title>Switches Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Switch
		</div>

		<div class="card-body">

  		<form action="{{route('switches.update', $switch->id)}}" method="post">
  			{{method_field('PATCH')}}
  			{{ csrf_field() }}
  		<div class="form-group">
				<label for="marca">Marca</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{$switch->Marca}}">
      @if ($errors->has('Marca'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('Marca') }}</strong>
          </span>
      @endif
                    </div>
                    </div>

		<div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{$switch->Caracteristicas}}">
      @if ($errors->has('Caracteristicas'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('Caracteristicas') }}</strong>
          </span>
      @endif
                    </div>
                    </div>

        <div class="form-group">
				<label for="ip_admin">IP Administrativa</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_admin') is-invalid  @enderror" name="ip_admin" value="{{$switch->ip_admin}}">
      @if ($errors->has('ip_admin'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('ip_admin') }}</strong>
          </span>
      @endif
                    </div>
                    </div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
