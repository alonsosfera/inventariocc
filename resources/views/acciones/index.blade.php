@extends('layouts.app')

@section('titulo')
  <title>Acciones</title>
@endsection
@section('content')

  <div class="container">
    <h1>Acciones de Mantenimiento</h1>

    <hr/>

    @if(session()->get('mensaje'))
      <p id="EditMessage" hidden>
        {{session()->get('mensaje')}}
      </p>
    @endif
    <div id="toolbar">
      <p id="ActiveRoute" hidden>/acciones/</p>
      <a href="{{route('acciones.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
      <button id="BtnEditar" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
      <button id="BtnEliminar" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    </div>
    <table
      id="tableInv"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-refresh="true"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-toolbar="#toolbar"
      data-single-select="true"
      data-click-to-select="true"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/lista"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="accion" data-sortable="true">Acción</th>
        </tr>
      </thead>

    </table>
  </div>
@endsection
