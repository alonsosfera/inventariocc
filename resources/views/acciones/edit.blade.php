@extends('layouts.app')

@section('titulo')
  <title>Acciones Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Accion de Mantenimiento
		</div>

		<div class="card-body">

  		<form action="{{route('acciones.update', $accion->id)}}" method="post">
  			{{method_field('PATCH')}}
  			{{ csrf_field() }}

  			<div class="form-group">
  				<label for="accion">Acción</label>
  				<div class="col">
  					<input type="text" class="form-control @error('accion') is-invalid @enderror" name="accion" value="{{$accion->nombre}}">

            @if ($errors->has('accion'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('accion') }}</strong>
                </span>
            @endif
        	</div>
  			</div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
