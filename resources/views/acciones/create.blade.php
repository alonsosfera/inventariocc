@extends('layouts.app')
@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Acción de Mantenimiento
	</div>
	<div class="card-body">
		<form action="{{route('acciones.store')}}" method="post">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="accion">Acción</label>
				<div class="col">
					<input type="text" class="form-control @error('accion') is-invalid @enderror" name="accion" value="{{old('accion')}}">

          @if ($errors->has('accion'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('accion') }}</strong>
              </span>
          @endif
      	</div>
			</div>

			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>




@endsection
