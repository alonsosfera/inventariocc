@extends('layouts.app')
@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Impresora
	</div>
	<div class="card-body">
		<form action="{{route('impresoras.store')}}" method="post">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="marca">Marca</label>
        <div class="col">
					<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{old('Marca')}}">
					@if ($errors->has('Marca'))
							<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('Marca') }}</strong>
							</span>
          @endif
        </div>
      </div>

			<div class="form-group">
				<label for="departamento">Departamento</label>
				<div class="col">
					<select  id="Departamento" name="Departamento" class="form-control @error('Departamento') is-invalid @enderror">
		                       <option value="" disabled selected>Selecciona un Departamento</option>
		                       @foreach($departamentos as $departamento)
		                       	<option value="{{$departamento->id}}">{{$departamento->nombre}}  </option>
		                       @endforeach
		     	</select>

					@if ($errors->has('Departamento'))
							<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('Departamento') }}</strong>
							</span>
          @endif
				</div>
			</div>


	     <div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
        <div class="col">
					<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{old('Caracteristicas')}}">
					@if ($errors->has('Caracteristicas'))
							<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('Caracteristicas') }}</strong>
							</span>
					@endif
          </div>
        </div>

			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>




@endsection
