@extends('layouts.app')

@section('titulo')
  <title>Impresoras Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Impresora
		</div>

		<div class="card-body">

  		<form action="{{route('impresoras.update', $impresora->id)}}" method="post">
  			{{method_field('PATCH')}}
  			{{ csrf_field() }}
        <div class="form-group">
  				<label for="marca">Marca</label>
          <div class="col">
  					<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{$impresora->Marca}}">
            @if ($errors->has('Marca'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Marca') }}</strong>
  							</span>
            @endif
          </div>
        </div>

  			<div class="form-group">
  				<label for="departamento">Departamento</label>
  				<div class="col">
  					<select  id="Departamento" name="Departamento" class="form-control @error('Departamento') is-invalid @enderror">
  		                       @foreach($departamentos as $departamento)
  		                       	<option value="{{$departamento->id}}" <?php if($impresora->Departamento==$departamento->id) echo 'selected="selected"'; ?>>{{$departamento->nombre}}  </option>
  		                       @endforeach
  		     	</select>

            @if ($errors->has('Departamento'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Departamento') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>


  	     <div class="form-group">
  				<label for="caracteristicas">Caracteristicas</label>
          <div class="col">
  					<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{$impresora->Caracteristicas}}">
            @if ($errors->has('Caracteristicas'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('Caracteristicas') }}</strong>
                </span>
            @endif
            </div>
          </div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
