@extends('layouts.app')

@section('titulo')
  <title>Impresoras</title>
@endsection
@section('content')

  @if(session()->get('mensaje'))
    <p id="EditMessage" hidden>
  		{{session()->get('mensaje')}}
  	</p>
  @endif
  <div class="container">
  	<h1>Impresoras</h1>

  	<hr/>
    <div id="toolbar">
      <p id="ActiveRoute" hidden>/impresoras/</p>
      @if (Auth::user()->tipo_cargo != 'Dirección')
        <a href="{{route('impresoras.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
        <button id="BtnEditar" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
        <button id="BtnEliminar" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></button>
      @endif
    </div>
    <table
      id="tableInv"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-export="true"
      data-export-types='["json", "xml", "csv", "txt", "pdf"]'
      data-export-options='{"fileName": "Impresoras", "jspdf": {"orientation":"landscape", "margins":{"left":30,"right":30,"bottom":30,"top":30}}}'
      data-show-refresh="true"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-toolbar="#toolbar"
      data-single-select="true"
      data-click-to-select="true"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listi"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="Marca" data-sortable="true">Marca</th>
          <th data-field="Caracteristicas" data-sortable="true">Caracteristicas</th>
          <th data-field="nombre" data-sortable="true">Departamento</th>
        </tr>
      </thead>

    </table>
  </div>
@endsection
