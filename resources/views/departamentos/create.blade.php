@extends('layouts.app')
@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Departamento
	</div>
	<div class="card-body">
		<form action="{{route('departamentos.store')}}" method="post">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="nombre">Nombre</label>
				<div class="col">
					<input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{old('nombre')}}">

					@if ($errors->has('nombre'))
							<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('nombre') }}</strong>
							</span>
					@endif
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>




@endsection
