@extends('layouts.app')

@section('titulo')
  <title>Departamentos</title>
@endsection
@section('content')

  @if(session()->get('mensaje'))
    <p id="EditMessage" hidden>
  		{{session()->get('mensaje')}}
  	</p>
  @endif
  <div class="container">
  	<h1>Departamentos</h1>

  	<hr/>
    <div id="toolbar">
      <p id="ActiveRoute" hidden>/departamentos/</p>
      @if (Auth::user()->tipo_cargo != 'Dirección')
        <a href="{{route('departamentos.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
        <button id="BtnMostrar" title="Mostrar" class="btn btn-secondary"><i class="fa fa-eye"></i></button>
        <button id="BtnEditar" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
        <button id="BtnEliminar" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></button>
      @endif
    </div>
    <table
      id="tableInv"
      data-toggle="table"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-refresh="true"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-toolbar="#toolbar"
      data-single-select="true"
      data-click-to-select="true"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listd"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="nombre" data-sortable="true">Nombre</th>
        </tr>
      </thead>

    </table>

  </div>
@endsection
