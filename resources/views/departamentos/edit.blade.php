@extends('layouts.app')

@section('titulo')
  <title>Departamentos Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Departamento
		</div>

		<div class="card-body">

  		<form action="{{route('departamentos.update', $departamento->id)}}" method="post">
  			{{method_field('PATCH')}}
  			{{ csrf_field() }}

  			<div class="form-group">
  				<label for="nombre">Nombre</label>
  				<div class="col">
  					<input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{$departamento->nombre}}">

  					@if ($errors->has('nombre'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('nombre') }}</strong>
  							</span>
  					@endif
  				</div>
  			</div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
