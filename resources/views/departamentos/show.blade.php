@extends('layouts.app')

@section('titulo')
  <title>{{$departamento->nombre}}</title>
@endsection
@section('content')
  <div class="container">
  	<h1>Inventario del Departamento de {{$departamento->nombre}}</h1>

  	<hr/>
    <h4>Computadoras</h4>
    <div id="toolbarC">
      <a href="{{route('computadoras.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
      <a href="javascript:;" id="EditarC" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="" id="EliminarC" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></a>
    </div>
    <table
      id="tableC"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-export="true"
      data-export-types='["json", "xml", "csv", "txt", "pdf"]'
      data-export-options='{"fileName": "ComputadorasDepartamento", "jspdf": {"orientation":"landscape", "margins":{"left":30,"right":30,"bottom":30,"top":30}}}'
      data-show-refresh="true"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-single-select="true"
      data-click-to-select="true"
      data-toolbar="#toolbarC"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listcd"
      data-query-params="queryParams"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="Nombre" data-sortable="true">Nombre</th>
          <th data-field="SO" data-sortable="true">S.O</th>
          <th data-field="Ram" data-sortable="true">Ram</th>
          <th data-field="Disco" data-sortable="true">Disco</th>
          <th data-field="Procesador" data-sortable="true">Procesador</th>
          <th data-field="Modelo" data-sortable="true">Modelo</th>
          <th data-field="Direccion_Ip" data-sortable="false">IP</th>
          <th data-field="Caracteristicas" data-sortable="false">Caracteristicas</th>
        </tr>
      </thead>

    </table><br><hr/>

    <h4>Impresoras</h4>
    <div id="toolbarI">
      <a href="{{route('impresoras.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
      <a href="javascript:;" id="EditarI" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="" id="EliminarI" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></a>
    </div>
    <table
      id="tableI"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-export="true"
      data-export-types='["json", "xml", "csv", "txt", "pdf"]'
      data-export-options='{"fileName": "ImpresorasDepartamento", "jspdf": {"orientation":"landscape", "margins":{"left":30,"right":30,"bottom":30,"top":30}}}'
      data-show-refresh="true"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-toolbar="#toolbarI"
      data-single-select="true"
      data-click-to-select="true"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listid"
      data-query-params="queryParams"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="Marca" data-sortable="true">Marca</th>
          <th data-field="Caracteristicas" data-sortable="true">Caracteristicas</th>
        </tr>
      </thead>

    </table>
  </div>
@endsection
