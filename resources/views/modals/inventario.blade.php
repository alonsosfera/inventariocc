<div class="modal fade" tabindex="-1" role="dialog" id="InventarioModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">INVENTARIO</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-comp-tab" data-toggle="tab" href="#nav-comp" role="tab" aria-controls="nav-comp" aria-selected="true">Computadoras</a>
            <a class="nav-item nav-link" id="nav-imp-tab" data-toggle="tab" href="#nav-imp" role="tab" aria-controls="nav-imp" aria-selected="false">Impresoras</a>
            <a class="nav-item nav-link" id="nav-rout-tab" data-toggle="tab" href="#nav-rout" role="tab" aria-controls="nav-rout" aria-selected="false">Routers</a>
            <a class="nav-item nav-link" id="nav-swit-tab" data-toggle="tab" href="#nav-swit" role="tab" aria-controls="nav-swit" aria-selected="false">Switches</a>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-comp" role="tabpanel" aria-labelledby="nav-home-tab">
            <div id="InvComp">
              <table
              id="tableComp"
              data-locale="es-MX"
              data-toggle="table"
              data-sort-class="table-active"
              data-sortable="true"
              data-search="true"
              data-single-select="true"
              data-click-to-select="true"
              data-page-list="[10, 25, 50, 100, All]"
              data-url="/listc"
              data-pagination="true">
                <thead>
                  <tr>
                    <th data-field="state" data-checkbox="true"></th>
                    <th data-field="id" data-sortable="true">ID</th>
                    <th data-field="Nombre" data-sortable="true">Nombre</th>
                    <th data-field="nombre" data-sortable="true">Departamento</th>
                    <th data-field="SO" data-sortable="true">S.O</th>
                    <th data-field="Ram" data-sortable="true">Ram</th>
                    <th data-field="Disco" data-sortable="true">Disco</th>
                    <th data-field="Procesador" data-sortable="true">Procesador</th>
                    <th data-field="Modelo" data-sortable="true">Modelo</th>
                    <th data-field="encargado" data-sortable="false">Encargado</th>
                    <th data-field="Direccion_Ip" data-sortable="false">IP</th>
                    <th data-field="Caracteristicas" data-sortable="false">Caracteristicas</th>
                  </tr>
                </thead>
              </table>
              <hr>
              <div class="pull-right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="SelectC()">Seleccionar</button>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-imp" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div id="InvImp">
            <table
              id="tableImp"
              data-locale="es-MX"
              data-toggle="table"
              data-sort-class="table-active"
              data-sortable="true"
              data-search="true"
              data-single-select="true"
              data-click-to-select="true"
              data-page-list="[10, 25, 50, 100, All]"
              data-url="/listi"
              data-pagination="true">
              <thead>
                <tr>
                  <th data-field="state" data-checkbox="true"></th>
                  <th data-field="id" data-sortable="true">ID</th>
                  <th data-field="Marca" data-sortable="true">Marca</th>
                  <th data-field="Caracteristicas" data-sortable="true">Caracteristicas</th>
                  <th data-field="nombre" data-sortable="true">Departamento</th>
                </tr>
              </thead>

            </table>
            <hr>
            <div class="pull-right">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="SelectI()">Seleccionar</button>
            </div>
            </div>

          </div>
          <div class="tab-pane fade" id="nav-rout" role="tabpanel" aria-labelledby="nav-contact-tab">
            <div id="InvRou">
            <table
              id="tableRou"
              data-locale="es-MX"
              data-toggle="table"
              data-sort-class="table-active"
              data-sortable="true"
              data-search="true"
              data-single-select="true"
              data-click-to-select="true"
              data-page-list="[10, 25, 50, 100, All]"
              data-url="/listr"
              data-pagination="true">
              <thead>
                <tr>
                  <th data-field="state" data-checkbox="true"></th>
                  <th data-field="id" data-sortable="true">ID</th>
                  <th data-field="Marca" data-sortable="true">Marca</th>
                  <th data-field="Caracteristicas" data-sortable="true">Caracteristicas</th>
                  <th data-field="ip_wan" data-sortable="true">IP WAN</th>
                  <th data-field="ip_lan" data-sortable="true">IP LAN</th>
                </tr>
              </thead>

            </table>
            <hr>
            <div class="pull-right">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="SelectR()">Seleccionar</button>
            </div>
            </div>

          </div>
          <div class="tab-pane fade" id="nav-swit" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div id="InvSwi">
            <table
              id="tableSwi"
              data-locale="es-MX"
              data-toggle="table"
              data-sort-class="table-active"
              data-sortable="true"
              data-search="true"
              data-page-list="[10, 25, 50, 100, All]"
              data-single-select="true"
              data-click-to-select="true"
              data-url="/lists"
              data-pagination="true">
              <thead>
                <tr>
                  <th data-field="state" data-checkbox="true"></th>
                  <th data-field="id" data-sortable="true">ID</th>
                  <th data-field="Marca" data-sortable="true">Marca</th>
                  <th data-field="Caracteristicas" data-sortable="true">Caracteristicas</th>
                  <th data-field="ip_admin" data-sortable="true">IP Administrativa</th>
                </tr>
              </thead>

            </table>
            <hr>
            <div class="pull-right">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="SelectS()">Seleccionar</button>
            </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
