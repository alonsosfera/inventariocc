<!-- Change pswd Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="TituloModal">Cambiar contraseña </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <div class="modal-body">
              <form id="PModal" method="POST" action="">
                {{ csrf_field() }}
                <div class="form-group">
          				<label for="password">{{ __('Password') }}</label>
                  <div class="col">
          					<input type="password" class="form-control @error ('password') is-invalid  @enderror" id="new-password" name="new-password" required>
                    @if ($errors->has('password'))
          	          <span class="invalid-feedback" role-"alert">
          	          	<strong>{{ $message}}</strong>
          	          </span>
                    @endif
                  </div>
                </div>

                <div class="form-group">
          				<label for="password-confirm">{{ __('Confirm Password') }}</label>
                  <div class="col">
          					<input id="new-password_confirmation" type="password" class="form-control" name="new-password_confirmation" required>
                  </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-6">
                        <button id="rp" type="submit" class="btn btn-primary">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
                </form>
        </div>
      </div>
    </div>
  </div>

    <!-- /Password Modal -->
