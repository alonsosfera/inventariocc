<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Sistema de Inventario') }}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.css">

    <!-- Toastr Alerts -->
    <link href="/css/toastr.css" rel="stylesheet"/>

    <!-- fontawesome 4.7>-->
    <link href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"  rel="stylesheet" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <!-- Scripts
    <script src="{{ asset('/js/app.js') }}" defer></script> -->
    <link href="/css/layout.css" rel="stylesheet" type="text/css" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
</head>
<body id="top">
    <div id="app" class="wrapper row0">
      <header id="header" class="hoc clear">
        <!-- ################################################################################################ -->
        <div id="logo">
          <h1><a href="/home">Inventario del Centro de Computo</a></h1>
          <i class="fa fa-ra"></i>
          <p>By EncomWeb delicias</p>
        </div>
        <!-- ################################################################################################ -->
      </header>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="wrapper row1">
      <nav id="mainav" class="hoc clear">
        <!-- ################################################################################################ -->
        <ul class="clear">
          @guest

          @else
          <li class="active"><a href="/home">Home</a></li>
          <li><a class="drop" href="#">Inventario</a>
            <ul>
              <li><a href="{{route('mantenimientos.index')}}">Mantenimientos</a></li>
              <li><a href="{{route('departamentos.index')}}">Departamentos</a></li>
              <li><a href="{{route('computadoras.index')}}">Computadoras</a></li>
              @if (Auth::user()->tipo_cargo == 'Administrador')
                <li><a href="{{route('acciones.index')}}">Acciones de Manenimiento</a></li>
              @endif
              <li><a href="{{route('impresoras.index')}}">Impresoras</a></li>
              <li><a href="{{route('routers.index')}}">Routers</a></li>
              <li><a href="{{route('switches.index')}}">Switches</a></li>
            </ul>
          </li>
          @if (Auth::user()->tipo_cargo != 'Servicio')
            <li><a href="/reportes">Reportes</a></li>
          @endif
          @if (Auth::user()->tipo_cargo == 'Administrador')
            <li><a href="/usuarios">Usuarios</a></li>
          @endif
          <li><a class="drop" href="#">{{ Auth::user()->username }}</a>
            <ul>
            <li><a href="#" data-toggle="modal" data-target="#modalPass" data-id="{{ Auth::user()->id }}">Cambiar contraseña</a></li>
              <li> <a class="dropdown-item" href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
              </form> </li>
            </ul>
            @endguest
          </li>
        </ul>
        <!-- ################################################################################################ -->
      </nav>
    </div>

        <main id="main">
            @yield('content')
        </main>
    </div>
    <div class="wrapper row4">
      <footer id="footer" class="hoc clear">
        <!-- ################################################################################################ -->
          <p class="fl_left">Copyright &copy; 2019 - Todos los derechos reservados - <a href="/licence">Inventario del Centro de Computo</a></p>

        <!-- ################################################################################################ -->
      </footer>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>

    <!-- MODALS -->
    @guest

    @else
      @include('modals.cpass')
    @endguest

    <!-- JAVASCRIPTS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.14.2/dist/extensions/export/bootstrap-table-export.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table-locale-all.min.js"></script>
    <script src="/js/jquery.backtotop.js"></script>
    <script src="/js/jquery.mobilemenu.js"></script>
    <!-- toastr notifications -->
    <script src="/js/toastr.js" type="text/javascript"></script>
    <!-- IE9 Placeholder Support -->
    <script src="/js/jquery.placeholder.min.js"></script>
    <!-- / IE9 Placeholder Support -->


    <script src="/js/main.js" type="text/javascript"></script>
</body>
</html>
