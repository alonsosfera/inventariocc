@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Reportes de Mantenimiento</h1>

  <hr/>
  @foreach($departamentos as $departamento)
    <div id="toolbar{{$departamento->nombre}}">
      <h4>{{$departamento->nombre}}</h4>
    </div>
    <table
      id="table"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-export="true"
      data-export-types='["json", "xml", "csv", "txt", "pdf"]'
      data-export-options='{"fileName": "ComputadorasDepartamento", "jspdf": {"orientation":"landscape", "margins":{"left":30,"right":30,"bottom":30,"top":30}}}'
      data-show-refresh="true"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-detail-view="true"
      data-detail-formatter="detailFormatter"
      data-search="true"
      data-toolbar="#toolbar{{$departamento->nombre}}"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listman"
      data-query-params="dep={{$departamento->nombre}}"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="id_Equipo" data-sortable="true">ID Equipo</th>
          <th data-field="tipo_equipo" data-sortable="true">Equipo</th>
          <th data-field="usuario" data-sortable="true">Encargado</th>
          <th data-field="acciones" data-sortable="true">Acciones</th>
          <th data-field="observaciones" data-sortable="true">Observaciones</th>
          <th data-field="created_at" data-sortable="true" data-formatter="FechaFormater">Fecha</th>
        </tr>
      </thead>

    </table><br><hr/>
  @endforeach
</div>
@endsection
