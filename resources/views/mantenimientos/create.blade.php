@extends('layouts.app')

@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

@if(session()->get('mensaje'))
	<p id="EditMessage" hidden>
		{{session()->get('mensaje')}}
	</p>
@endif

<div class="container">
	<div class="card-header">
		Registrar Mantenimiento
	</div>
	<div class="card-body">

		<form action="{{route('mantenimientos.store')}}" method="post">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="equipo">Equipo</label>
				<div class="col">
					<button type="button" id="equipobtn" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#InventarioModal">Seleccionar Equipo</button>
					<input id="id_Equipo" type="text" class="form-control @error('id_Equipo') is-invalid @enderror" name="id_Equipo" value="{{old('id_Equipo')}}" hidden>
					<input id="tipo_equipo" type="text" class="form-control @error('tipo_equipo') is-invalid @enderror" name="tipo_equipo" value="{{old('tipo_equipo')}}" hidden>
					<input id="deptin" type="text" class="form-control" name="departamento" value="{{old('departamento')}}" hidden>

          @if ($errors->has('id_Equipo'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('id_Equipo') }}</strong>
              </span>
          @endif
      	</div>
			</div>

			<div class="form-group">
				<p>Acciones realizadas</p>
				<div class="col text-center">
					@foreach($acciones as $accion)
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" class="custom-control-input" name="acciones[]" id="check{{$accion->accion}}" value="{{$accion->accion}}">
							<label class="custom-control-label" for="check{{$accion->accion}}">{{$accion->accion}}</label>
						</div>
				 	@endforeach
				</div>
			</div>

			<div class="form-group">
				<label for="observaciones">Observaciones</label>
				<div class="col">
					<textarea type="text"rows="3" cols="60" class="form-control @error('observaciones') is-invalid @enderror" name="observaciones" value="{{old('observaciones')}}"></textarea>

          @if ($errors->has('observaciones'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('observaciones') }}</strong>
              </span>
          @endif
      	</div>
			</div>

			<button type="submit" class="btn btn-primary">Agregar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div><br><hr/>
</div>
<!-- MODALS -->
@guest

@else
	@include('modals.inventario')
@endguest
@endsection
