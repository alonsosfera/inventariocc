@extends('layouts.app')

@section('titulo')
  <title>Mantenimiento</title>
@endsection
@section('content')
  <div class="container">
  	<h1>Mantenimiento</h1>

  	<hr/>

    @if(session()->get('mensaje'))
      <p id="EditMessage" hidden>
    		{{session()->get('mensaje')}}
    	</p>
    @endif
    <div id="toolbar">
      <p id="ActiveRoute" hidden>/mantenimientos/</p>
      @if (Auth::user()->tipo_cargo != 'Dirección')
        <a href="{{route('mantenimientos.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
      @endif
      @if (Auth::user()->tipo_cargo == 'Administrador')
        <button id="BtnEliminar" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></button>
      @endif
    </div>
    <table
      id="tableInv"
      data-locale="es-MX"
      data-show-footer="true"
      data-show-export="true"
      data-export-types='["json", "xml", "csv", "txt", "pdf"]'
      data-export-options='{"fileName": "Mantenimientos", "jspdf": {"orientation":"landscape", "margins":{"left":30,"right":30,"bottom":30,"top":30}}}'
      data-show-refresh="true"
      data-toggle="table"
      data-detail-view="true"
      data-detail-formatter="detailFormatter"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-toolbar="#toolbar"
      data-single-select="true"
      data-click-to-select="true"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listm"
      data-pagination="true">
      <thead>
        <tr>
          @if (Auth::user()->tipo_cargo == 'Administrador')
            <th data-field="state" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true">ID</th>
          @endif
          <th data-field="id_Equipo" data-sortable="true">ID Equipo</th>
          <th data-field="tipo_equipo" data-sortable="true">Equipo</th>
          <th data-field="usuario" data-sortable="true">Encargado</th>
          <th data-field="created_at" data-sortable="true" data-formatter="FechaFormater">Fecha</th>
        </tr>
      </thead>

    </table>
  </div>
@endsection
