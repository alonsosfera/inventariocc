@extends('layouts.app')
@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Router
	</div>
	<div class="card-body">
		<form action="{{route('routers.store')}}" method="post">
			{{ csrf_field() }}

			 <div class="form-group">
				<label for="marca">Marca</label>
        <div class="col">
					<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{old('Marca')}}">
					@if ($errors->has('Marca'))
							<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('Marca') }}</strong>
							</span>
          @endif
        </div>
      </div>

			<div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{old('Caracteristicas')}}">
			@if ($errors->has('Caracteristicas'))
					<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('Caracteristicas') }}</strong>
					</span>
			@endif
                    </div>
                    </div>

            <div class="form-group">
				<label for="ip_wan">IP WAN</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_wan') is-invalid  @enderror" name="ip_wan" value="{{old('ip_wan')}}">
			@if ($errors->has('ip_wan'))
					<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('ip_wan') }}</strong>
					</span>
			@endif
                    </div>
                    </div>

            <div class="form-group">
				<label for="ip_lan">IP LAN</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_lan') is-invalid  @enderror" name="ip_lan" value="{{old('ip_lan')}}">
			@if ($errors->has('ip_lan'))
					<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('ip_lan') }}</strong>
					</span>
			@endif
                    </div>
                    </div>

			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>




@endsection
