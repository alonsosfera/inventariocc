@extends('layouts.app')

@section('titulo')
  <title>Computadoras</title>
@endsection
@section('content')

  @if(session()->get('mensaje'))
    <p id="EditMessage" hidden>
  		{{session()->get('mensaje')}}
  	</p>
  @endif
  <div class="container">
  	<h1>Computadoras</h1>

  	<hr/>
    <div id="toolbar">
      <p id="ActiveRoute" hidden>/computadoras/</p>
      @if (Auth::user()->tipo_cargo != 'Dirección')
        <a href="{{route('computadoras.create')}}" title="Nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> </a>
        <button id="BtnEditar" title="Editar" class="btn btn-success"><i class="fa fa-edit"></i></button>
        <button id="BtnEliminar" title="Eliminar" class="btn btn-danger"><i class="fa fa-trash"></i></button>
      @endif
    </div>
    <table
      id="tableInv"
      data-show-footer="true"
      data-show-export="true"
      data-export-types='["json", "xml", "csv", "txt", "pdf"]'
      data-export-options='{"fileName": "Computadoras", "jspdf": {"orientation":"landscape", "margins":{"left":30,"right":30,"bottom":30,"top":30}}}'
      data-show-refresh="true"
      data-locale="es-MX"
      data-toggle="table"
      data-sort-class="table-active"
      data-sortable="true"
      data-search="true"
      data-single-select="true"
      data-click-to-select="true"
      data-toolbar="#toolbar"
      data-page-list="[10, 25, 50, 100, All]"
      data-url="/listc"
      data-pagination="true">
      <thead>
        <tr>
          <th data-field="state" data-checkbox="true"></th>
          <th data-field="id" data-sortable="true">ID</th>
          <th data-field="Nombre" data-sortable="true">Nombre</th>
          <th data-field="nombre" data-sortable="true">Departamento</th>
          <th data-field="SO" data-sortable="true">S.O</th>
          <th data-field="Ram" data-sortable="true">Ram</th>
          <th data-field="Disco" data-sortable="true">Disco</th>
          <th data-field="Procesador" data-sortable="true">Procesador</th>
          <th data-field="Modelo" data-sortable="true">Modelo</th>
          <th data-field="encargado" data-sortable="false">Encargado</th>
          <th data-field="Direccion_Ip" data-sortable="false">IP</th>
          <th data-field="Caracteristicas" data-sortable="false">Caracteristicas</th>
        </tr>
      </thead>

    </table>

  </div>
@endsection
