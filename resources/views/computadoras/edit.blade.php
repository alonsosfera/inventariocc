@extends('layouts.app')

@section('titulo')
  <title>Computadoras Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Computadora
		</div>

		<div class="card-body">

  		<form action="{{route('computadoras.update', $computadora->id)}}" method="post">
  			{{method_field('PATCH')}}
  			{{ csrf_field() }}
        <div class="form-group">
  				<label for="Nombre">Nombre</label>
  				<div class="col">
  					<input type="text" class="form-control @error('Nombre') is-invalid @enderror" name="Nombre" value="{{$computadora->Nombre}}">

            @if ($errors->has('Nombre'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Nombre') }}</strong>
  							</span>
            @endif
        	</div>
  			</div>

  			<div class="form-group">
  				<label for="departamento">Departamento</label>
  				<div class="col">
  					<select  id="Departamento" name="Departamento" class="form-control @error('Departamento') is-invalid @enderror">
  		                       @foreach($departamentos as $departamento)
  		                       	<option value="{{$departamento->id}}"<?php if($computadora->Departamento==$departamento->id) echo 'selected="selected"'; ?>>{{$departamento->nombre}}  </option>
  		                       @endforeach
  		     	</select>

            @if ($errors->has('Departamento'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Departamento') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="SO">Sistema Operativo</label>

  				<div class="col">
  					<input type="text" class="form-control @error('SO') is-invalid @enderror" name="SO" value="{{$computadora->SO}}">

            @if ($errors->has('SO'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('SO') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="ram">Memoria Ram</label>
  				<div class="col">
  					<input type="text" class="form-control @error('Ram') is-invalid @enderror" name="Ram" value="{{$computadora->Ram}}">

            @if ($errors->has('Ram'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Ram') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="disco">Disco Duro</label>
  				<div class="col">
  					<input type="text" class="form-control @error('Disco') is-invalid @enderror" name="Disco" value="{{$computadora->Disco}}">

            @if ($errors->has('Disco'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Disco') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="procesador">Procesador</label>
  				<div class="col">
  					<input type="text" class="form-control @error('Procesador') is-invalid @enderror" name="Procesador" value="{{$computadora->Procesador}}">

            @if ($errors->has('Procesador'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Procesador') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="modelo">Modelo</label>
  				<div class="col">
  					<input type="text" class="form-control @error('Modelo') is-invalid @enderror" name="Modelo" value="{{$computadora->Modelo}}">

            @if ($errors->has('Modelo'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Modelo') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="caracteristicas">Caracteristicas</label>
  				<div class="col">
  					<input type="text" class="form-control @error('Caracteristicas') is-invalid @enderror" name="Caracteristicas" value="{{$computadora->Caracteristicas}}">

            @if ($errors->has('Caracteristicas'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('Caracteristicas') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="encargado">Encargado</label>
  				<div class="col">
  					<input type="text" class="form-control @error('encargado') is-invalid @enderror" name="encargado" value="{{$computadora->encargado}}">

            @if ($errors->has('encargado'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('encargado') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<div class="form-group">
  				<label for="ip">Dirección IP</label>
  				<div class="col">
  					<input type="text" class="form-control @error('ip') is-invalid @enderror" name="ip" value="{{$computadora->Direccion_Ip}}">

            @if ($errors->has('ip'))
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $errors->first('ip') }}</strong>
  							</span>
            @endif
  				</div>
  			</div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
