//Inicio de codigo JavaScript

  var $table = $('#tableInv')
  var $tableI = $('#tableI')
  var $tableC = $('#tableC')


  function queryParams(params) {
    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    params.id = id // add param1
    return params
  }

  $(function() {

    if($('#EditMessage').text().includes('Actualizado')){
      toastr.success('Actualizado correctamente', 'Exito!', {timeOut: 3000});
    }else if ($('#EditMessage').text().includes('Agregado')) {
      toastr.success('Creado correctamente', 'Exito!', {timeOut: 3000});
    }

    function GetSelected(){
      var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      return ids;
    }

    $('#tableI').click(function(){
      var ids = $.map($tableI.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      $('#EditarI').attr('href', '/impresoras/'+ids+'/edit');
      $('#EliminarI').attr('href', ids);
    })

    $('#tableC').click(function(){
      var ids = $.map($tableC.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      $('#EditarC').attr('href', '/computadoras/'+ids+'/edit');
      $('#EliminarC').attr('href', ids);
    })

    $('#EliminarC').click(function(e){
      e.preventDefault();
      if($('#EliminarC').attr('href')==""){
        return;
      }
      if (window.confirm("¿Estas seguro?")) {
        $.post({
            type: 'DELETE',
            url: '/computadoras/'+$('#EliminarC').attr('href'),
            data: {  _token: $('meta[name="csrf-token"]').attr('content') },
        }).done(function (data) {
          $tableC.bootstrapTable('remove', {
            field: 'id',
            values: $('#EliminarC').attr('href')
          })
          toastr.success(data, 'Exito!', {timeOut: 3000});
        });
      }
    })

    $('#EliminarI').click(function(e){
      e.preventDefault();
      if($('#EliminarI').attr('href')==""){
        return;
      }
      if (window.confirm("¿Estas seguro que deseas eliminar?")) {
        $.post({
            type: 'DELETE',
            url: '/impresoras/'+$('#EliminarI').attr('href'),
            data: {  _token: $('meta[name="csrf-token"]').attr('content') },
        }).done(function (data) {
          $tableI.bootstrapTable('remove', {
            field: 'id',
            values: $('#EliminarI').attr('href')
          })
          toastr.success(data, 'Exito!', {timeOut: 3000});
        });
      }
    })

    $('#BtnEliminar').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      if (window.confirm("¿Estas seguro que deseas eliminar?")) {
        $table.bootstrapTable('remove', {
          field: 'id',
          values: id
        })
        $.ajax({
          type: 'DELETE',
          url: $('#ActiveRoute').text()+id,
          data: {  _token: $('meta[name="csrf-token"]').attr('content') },
          success: function(data){
            toastr.success(data, 'Exito!', {timeOut: 3000});
          }
        });
      }
    })

    $('#BtnEditar').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      $(location).attr("href", $('#ActiveRoute').text()+id+'/edit');
    })

    $('#BtnMostrar').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      $(location).attr("href", 'departamentos/'+id);
    })

    $('#BtnPsw').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      var usuario = $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.username
      })
      if (window.confirm("¿Restablecer contraseña de "+usuario+"?")) {
        $.ajax({
          type: 'post',
          url: '/rusuarios/'+id,
          data: {  _token: $('meta[name="csrf-token"]').attr('content') },
          success: function(data){
            toastr.success(data, 'Exito!', {timeOut: 3000});
          }
        });
      }
    })

    $('#BtnMision').click(function(){
      var mision = $('#mision').text();
      var vision = $('#vision').text();
      $.ajax({
        type:'post',
        url: 'mision',
        data: {  _token: $('meta[name="csrf-token"]').attr('content'), mision: mision, vision: vision},
        success: function(data){
          toastr.success(data, 'Exito!', {timeOut: 3000});
        }
      });
    })

    $('#rp').on('click', function(e) {
      e.preventDefault();
      var inputs=$('#PModal').serializeArray();
      var datos = {};
      $.each(inputs, function (i, input) {
        datos[input.name] = input.value;
      });
      datos['_token'] = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
          type: 'POST',
          url: '/cp',
          dataType:'json',
          data:datos,
          success: function(resultado) {
            document.getElementById("PModal").reset();
            if(resultado.Exito != undefined)
            {
                toastr.success(resultado.Exito, 'Exito!', {timeOut: 4000});
                $('#modalPass').modal('hide');
            }else{
                toastr.warning(resultado.Error, 'Alerta!', {timeOut: 4000});
            }
          },
          error: function(reject){
            console.log(reject);
            if( reject.status === 422 ) {
                Validate(reject.responseJSON);
            }
          }
      });
    });

  })

  function FechaFormater(value) {
    var options = {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', hour12:false, minute: 'numeric'};
    let date = new Date(value);
    return date.toLocaleString("es-MX", options);
    //return date.getDay()+'/'+monthNames[date.getMonth()]+'/'+date.getFullYear()+' - ';
  }

  function detailFormatter(index, row){
    var html = []

    html.push('<p><b>Acciones de mantenimiento:</b> ' + row.acciones + '</p>')
    html.push('<p><b>Observaciones:</b> ' + row.observaciones + '</p>')
    return html.join('')
  }

  function SelectC(){
    var row = $.map($('#tableComp').bootstrapTable('getSelections'), function (row) {
      return row
    })
    if(row != ""){
      $('#InventarioModal').modal('hide');
      $('#equipobtn').text('Computadora: '+row[0].Nombre);
      $('#equipobtn').removeClass('btn-secondary').addClass('btn-success');
      $('#id_Equipo').val(row[0].id);
      $('#tipo_equipo').val('Computadora');
      $('#deptin').val(row[0].nombre);

    }
  }

  function SelectR(){
    var row = $.map($('#tableRou').bootstrapTable('getSelections'), function (row) {
      return row
    })
    if(row != ""){
      $('#InventarioModal').modal('hide');
      $('#equipobtn').text('Router: '+row[0].id+' - '+row[0].Marca);
      $('#equipobtn').removeClass('btn-secondary').addClass('btn-success');
      $('#id_Equipo').val(row[0].id);
      $('#tipo_equipo').val('Router');
      $('#deptin').val('Centro de Computo');

    }
  }

  function SelectI(){
    var row = $.map($('#tableImp').bootstrapTable('getSelections'), function (row) {
      return row
    })
    if(row != ""){
      $('#InventarioModal').modal('hide');
      $('#equipobtn').text('Impresora: '+row[0].id+' - '+row[0].Marca);
      $('#equipobtn').removeClass('btn-secondary').addClass('btn-success');
      $('#id_Equipo').val(row[0].id);
      $('#tipo_equipo').val('Impresora');
      $('#deptin').val(row[0].nombre);

    }
  }

  function SelectS(){
    var row = $.map($('#tableSwi').bootstrapTable('getSelections'), function (row) {
      return row
    })
    if(row != ""){
      $('#InventarioModal').modal('hide');
      $('#equipobtn').text('Switch: '+row[0].id+' - '+row[0].Marca);
      $('#equipobtn').removeClass('btn-secondary').addClass('btn-success');
      $('#id_Equipo').val(row[0].id);
      $('#tipo_equipo').val('Switch');
      $('#deptin').val('Centro de Computo');

    }
  }
