<?php

namespace App\Http\Controllers;

use App\Mantenimiento;
use App\Accion;
use App\Departamento;
use Illuminate\Http\Request;

class MantenimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('mantenimientos.index');
    }

    public function table()
    {
        $mantenimientos = Mantenimiento::all();
        return $mantenimientos;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $acciones = Accion::all();
        return view('mantenimientos.create', compact('acciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_Equipo' => 'required',
            'tipo_equipo'=> 'required',
            'observaciones'=> 'required'
        ]);

        $acciont ="";
        $acciones = $request->get('acciones');
        foreach($acciones as $accion){
          $acciont = $acciont.$accion.", ";
        }
        $mantenimiento = new Mantenimiento([
            'id_Equipo'=>$request->get('id_Equipo'),
            'tipo_equipo'=> $request->get('tipo_equipo'),
            'departamento'=> $request->get('departamento'),
            'usuario'=> \Auth::user()->username,
            'acciones'=> $acciont,
            'observaciones'=> $request->get('observaciones')
        ]);
        $mantenimiento->save();
        $acciones = Accion::all();
        return redirect('/mantenimientos/create')->with([
                                                          'mensaje' => 'Agregado con exito',
                                                          'acciones' => $acciones
                                                      ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Mantenimiento::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $mantenimiento = Mantenimiento::findOrFail($id);
        return view('mantenimientos.edit', compact('mantenimiento'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mantenimiento $mantenimiento)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mantenimiento = Mantenimiento::find($id);
        $mantenimiento->delete();

        return "Eliminado correctamente";
    }

    public function reportes()
    {
      if(\Auth::user()->tipo_cargo == 'Servicio'){
        return redirect('home');
      }
      $departamentos = Departamento::all();
      return view('mantenimientos.reportes', compact('departamentos'));
    }

    public function listR(Request $request)
    {
      if(\Auth::user()->tipo_cargo == 'Servicio'){
        return redirect('home');
      }
      $departamento = $request->get('dep');
      $mantenimiento = Mantenimiento::where('departamento', '=', $departamento)->latest('created_at')->get();
      return $mantenimiento;

    }
}
