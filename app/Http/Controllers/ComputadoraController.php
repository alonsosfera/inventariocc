<?php

namespace App\Http\Controllers;

use App\Computadora;
use App\Departamento;
use Illuminate\Http\Request;

class ComputadoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   public function __construct()
   {
       $this->middleware('auth');
   }

   public function new()
   {
       return view('computadoras.new');
   }

    public function index()
    {
        return view('computadoras.index');
    }

    public function table()
    {
      $computadoras = \DB::table('computadoras')
            ->join('departamentos', 'computadoras.departamento', '=', 'departamentos.id')
            ->select('computadoras.id','computadoras.Nombre','departamentos.nombre','computadoras.SO','computadoras.Ram','computadoras.Disco','computadoras.Modelo','computadoras.Procesador','computadoras.Caracteristicas','computadoras.Direccion_Ip','computadoras.encargado')
            ->get();
      return $computadoras;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $departamentos = Departamento::all();
        return view('computadoras.create', compact('departamentos'));

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'Nombre' => 'required',
            'SO'=> 'required',
            'Ram'=> 'required',
            'Disco'=> 'required',
            'Procesador'=> 'required',
            'Modelo'=> 'required',
            'Departamento'=> 'required',
            'Caracteristicas'=> 'required',
            'encargado'=> 'required',
            'ip'=> 'required | ip'
        ]);


        $computadora = new Computadora([
            'Nombre'=>$request->get('Nombre'),
            'SO'=> $request->get('SO'),
            'Ram'=> $request->get('Ram'),
            'Disco'=> $request->get('Disco'),
            'Procesador'=> $request->get('Procesador'),
            'Modelo'=> $request->get('Modelo'),
            'Departamento'=> $request->get('Departamento'),
            'Caracteristicas'=> $request->get('Caracteristicas'),
            'Direccion_Ip'=> $request->get('ip'),
            'encargado'=> $request->get('encargado')

        ]);
        $computadora->save();
        return redirect('computadoras')->with('mensaje', 'Agregado con exito' );


      }

    /**
     * Display the specified resource.
     *
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request)
     {
       $id = $request->get('id');
       $computadoras = \DB::table('computadoras')
             ->where('departamento', '=', $id)
             ->select('computadoras.*')
             ->get();
       return $computadoras;
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //editar computadpra
        $computadora = Computadora::findOrFail($id);
        $departamentos = Departamento::all();
        $departamento = Departamento::findOrFail($computadora->Departamento);
        return view('computadoras.edit', compact('computadora','departamentos','departamento'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
                $request->validate([
            'Nombre' => 'required',
            'SO'=> 'required',
            'Ram'=> 'required',
            'Disco'=> 'required',
            'Procesador'=> 'required',
            'Modelo'=> 'required',
            'Departamento'=> 'required',
            'Caracteristicas'=> 'required',
            'Direccion_Ip'=> $request->get('ip'),
            'encargado'=> 'required'
        ]);

        $computadora = Computadora::find($id);
        $computadora->Nombre= $request->get('Nombre');
        $computadora->SO = $request->get('SO');
        $computadora->Ram= $request->get('Ram');
        $computadora->Disco= $request->get('Disco');
        $computadora->Procesador= $request->get('Procesador');
        $computadora->Modelo= $request->get('Modelo');
        $computadora->Departamento= $request->get('Departamento');
        $computadora->Caracteristicas= $request->get('Caracteristicas');
        $computadora->Direccion_Ip= $request->get('ip');
        $computadora->encargado= $request->get('encargado');

       $computadora->save();

        return redirect('computadoras')->with('mensaje', 'Actualizado con exito' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $computadora = Computadora::find($id);
        $computadora->delete();
        return "Eliminado correctamente";

    }
}
