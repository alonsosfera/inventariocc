<?php

namespace App\Http\Controllers;

use App\Impresora;
use App\Departamento;
use Illuminate\Http\Request;

class ImpresoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }

    public function index()
    {
      //metodo de listar datos
      return view('impresoras.index');
    }

    public function table()
    {
        $impresoras = \DB::table('impresoras')
                ->join('departamentos', 'impresoras.departamento', '=', 'departamentos.id')
              ->select('impresoras.id','impresoras.Marca','impresoras.Caracteristicas','departamentos.nombre')
              ->get();
        return $impresoras;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $departamentos = Departamento::all();
        return view('impresoras.create', compact('departamentos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'Marca'=> 'required',
            'Caracteristicas'=> 'required',
            'Departamento'=> 'required | integer'
        ]);


        $impresora = new Impresora([
            'Marca'=> $request->get('Marca'),
                       'Caracteristicas'=> $request->get('Caracteristicas'),
            'Departamento'=> $request->get('Departamento')

        ]);
        $impresora->save();
        return redirect('impresoras')->with('mensaje', 'Agregado con exito' );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Impresora  $impresora
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->get('id');
        $impresoras = \DB::table('impresoras')
              ->where('departamento', '=', $id)
              ->select('impresoras.*')
              ->get();
        return $impresoras;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Impresora  $impresora
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $impresora = Impresora::findOrFail($id);
        $departamentos = Departamento::all();
        $departamentoImpresoras = Departamento::findOrFail($impresora->Departamento);


        return view('impresoras.edit', compact('impresora', 'departamentos','departamentoImpresoras'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Impresora  $impresora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'Marca'=> 'required',
            'Caracteristicas'=> 'required',
            'Departamento'=> 'required | integer'
        ]);


        $impresora = Impresora::find($id);
        $impresora->Marca = $request->get('Marca');
        $impresora->Caracteristicas = $request->get('Caracteristicas');
        $impresora->Departamento = $request->get('Departamento');

       $impresora->save();

        return redirect('impresoras')->with('mensaje', 'Actualizado con exito' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Impresora  $impresora
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $impresora = Impresora::find($id);
        $impresora->delete();
        return "Eliminado correctamente";

    }
}
