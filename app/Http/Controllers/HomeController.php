<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $mision = \DB::table('mision')->where('id', 0)->first();
        return view('home', compact('mision'));
    }

    public function mision(Request $request)
    {
      $mision = \DB::table('mision')->where('id', 0)->update(['mision' => $request->get('mision'), 'vision' => $request->get('vision')]);
      return "Misión y visión actualizadas";
    }

   public function users()
   {
       if(\Auth::user()->tipo_cargo != 'Administrador'){
         return redirect('home');
       }
       return view('usuarios.index');
   }

   public function table()
   {
       if(\Auth::user()->tipo_cargo != 'Administrador'){
         return redirect('home');
       }
       $users = User::all();
       return $users;
   }

    public function editUser($id)
    {
        //editar usuario
        if(\Auth::user()->tipo_cargo != 'Administrador'){
          return redirect('home');
        }
        $usuario = User::findOrFail($id);
        return view('usuarios.edit', compact('usuario'));

    }

    public function updateUser(Request $request, $id)
    {
        //
        if(\Auth::user()->tipo_cargo != 'Administrador'){
          return redirect('home');
        }
            $request->validate([
            'username' => 'required',
            'cargo'=> 'required'
        ]);

        $usuario = User::find($id);
        $usuario->username= $request->get('username');
        $usuario->tipo_cargo = $request->get('cargo');

        $usuario->save();

        return redirect('usuarios')->with('mensaje', 'Actualizado con exito' );
    }

    public function destroy($id)
    {
        //
        if(\Auth::user()->tipo_cargo != 'Administrador'){
          return redirect('home');
        }
        $usuario = User::find($id);
        $usuario->delete();
        return "Eliminado correctamente";

    }

    public function RestorePass($id)
    {
      if(\Auth::user()->tipo_cargo != 'Administrador'){
        return redirect('home');
      }
      $usuario = User::find($id);
      $usuario->password = \Hash::make($usuario->username);
      return "Contraseña restablecida";
    }

    public function CP(Request $request){
        $this->validate($request, [
            'new-password' => 'required|string|min:6|confirmed|same:new-password_confirmation',
            'new-password_confirmation' => 'required|string|min:6'
        ]);
        //Change Password
        $user = \Auth::user();
        $user->password = \Hash::make($request->input('new-password'));
        $user->save();
        $data = array(
         'Exito'  => 'Cambio de contraseña satisfactorio !'
        );
        return json_encode($data);
    }
}
