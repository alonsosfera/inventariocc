<?php

namespace App\Http\Controllers;

use App\Accion;
use Illuminate\Http\Request;

class AccionesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

 public function __construct()
 {
     $this->middleware('auth');
 }

  public function index()
  {
      return view('acciones.index');
  }

  public function table()
  {
      $Acciones = Accion::all();
      return $Acciones;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('acciones.create');

      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $request->validate([
          'accion' => 'required'
      ]);

      $Accion = new Accion([
          'accion'=> $request->get('accion')

      ]);
      $Accion->save();
      return redirect('acciones')->with('mensaje', 'Agregado con exito' );
    }


      //
      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
          return Accion::find($id);
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          //
          $accion = Accion::findOrFail($id);
          return view('acciones.edit', compact('accion'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
          //
          $request->validate([
              'accion' => 'required'
          ]);

          $accion = Accion::find($id);
          $accion->accion= $request->get('accion');
          $accion->save();

          return redirect('acciones')->with('mensaje', 'Actualizado con exito' );
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
       // public function destroy(Computadora $computadora)
     public function destroy($id)
      {
          //
          $accion = Accion::find($id);
          $accion->delete();
          return "Eliminado correctamente";

      }
}
