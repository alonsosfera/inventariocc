<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mantenimiento extends Model
{
	//
	protected $table = 'mantenimiento';
    protected $fillable=[
    	'id',
    	'id_Equipo',
    	'tipo_equipo',
    	'usuario',
    	'acciones',
    	'observaciones',
    	'departamento'
	];

	public function departamento() {
        return $this->hasOne('App\Departamento', 'id', 'Departamento');
	}

	public function usuario(){
		return $this->hasOne('App\User', 'id', 'usuario');

	}

}
