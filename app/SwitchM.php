<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwitchM extends Model
{
    //
   protected $table=('switches');

   protected $fillable=[
    'id',
    'Marca',
    'Caracteristicas',
    'ip_admin'
  ];
}
