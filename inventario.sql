-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2019 at 01:56 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventario`
--

-- --------------------------------------------------------

--
-- Table structure for table `accions`
--

CREATE TABLE `accions` (
  `id` int(10) UNSIGNED NOT NULL,
  `accion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accions`
--

INSERT INTO `accions` (`id`, `accion`, `created_at`, `updated_at`) VALUES
(2, 'Limpieza', '2019-06-01 03:38:36', '2019-06-01 03:38:36'),
(3, 'Formateo', '2019-06-01 17:28:14', '2019-06-01 17:28:14'),
(4, 'Actualización', '2019-06-01 17:41:25', '2019-06-01 17:41:25'),
(5, 'Reparación', '2019-06-01 17:41:34', '2019-06-01 17:41:34'),
(6, 'Reemplazo', '2019-06-01 17:41:54', '2019-06-01 17:41:54'),
(7, 'Otra accion', '2019-06-01 17:42:10', '2019-06-01 17:42:10');

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE `cargo` (
  `id` smallint(6) NOT NULL,
  `Tipo_Cargo` varchar(35) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `computadoras`
--

CREATE TABLE `computadoras` (
  `id` smallint(6) NOT NULL,
  `Nombre` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `SO` varchar(20) DEFAULT NULL,
  `Ram` varchar(20) NOT NULL COMMENT 'GB',
  `Disco` varchar(20) NOT NULL COMMENT 'GB',
  `Procesador` varchar(35) NOT NULL,
  `Modelo` varchar(30) NOT NULL,
  `Departamento` smallint(20) UNSIGNED NOT NULL,
  `Caracteristicas` varchar(50) NOT NULL,
  `Direccion_Ip` varchar(20) NOT NULL,
  `encargado` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `computadoras`
--

INSERT INTO `computadoras` (`id`, `Nombre`, `SO`, `Ram`, `Disco`, `Procesador`, `Modelo`, `Departamento`, `Caracteristicas`, `Direccion_Ip`, `encargado`, `created_at`, `updated_at`) VALUES
(4, 'LMM01', 'Ubuntu', '4GB', '500GB', 'Core I5', 'HP', 1, 'Escritorio', '192.168.1.1', 'Pedro', '2019-06-01 02:51:13', '2019-06-01 02:51:13'),
(5, 'LMM02', 'Windows', '4GB', '500GB', 'Core I5', 'HP', 2, 'Escritorio', '192.168.1.2', 'Pedro', '2019-06-01 02:55:21', '2019-06-01 02:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `departamentos`
--

CREATE TABLE `departamentos` (
  `id` smallint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departamentos`
--

INSERT INTO `departamentos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Sistemas', '2019-05-21 23:08:12', '2019-05-21 23:08:12'),
(2, 'Metalmecanica', '2019-05-23 00:29:33', '2019-05-23 00:29:33');

-- --------------------------------------------------------

--
-- Table structure for table `impresoras`
--

CREATE TABLE `impresoras` (
  `id` smallint(6) NOT NULL,
  `Marca` varchar(20) NOT NULL,
  `Caracteristicas` varchar(50) NOT NULL,
  `Departamento` smallint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `impresoras`
--

INSERT INTO `impresoras` (`id`, `Marca`, `Caracteristicas`, `Departamento`, `created_at`, `updated_at`) VALUES
(1, 'HP', 'Escritorio', 1, '2019-05-21 23:41:16', '2019-05-21 23:41:16'),
(2, 'HP', 'Escritorio', 1, '2019-05-29 02:15:33', '2019-05-29 02:15:33');

-- --------------------------------------------------------

--
-- Table structure for table `mantenimiento`
--

CREATE TABLE `mantenimiento` (
  `id` smallint(6) NOT NULL,
  `id_Equipo` smallint(20) UNSIGNED NOT NULL,
  `departamento` varchar(25) NOT NULL,
  `tipo_equipo` varchar(20) NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `acciones` varchar(255) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mantenimiento`
--

INSERT INTO `mantenimiento` (`id`, `id_Equipo`, `departamento`, `tipo_equipo`, `usuario`, `acciones`, `observaciones`, `created_at`, `updated_at`) VALUES
(8, 2, 'Sistemas', 'Impresora', 'Prueba', 'Formateo, Reemplazo, ', 'Se cambio la impresora, ya que tiene un puerto no funcional.', '2019-06-01 21:03:00', '2019-06-01 21:03:00'),
(9, 4, 'Centro de Computo', 'Router', 'Prueba', 'Formateo, ', 'F', '2019-06-02 23:07:58', '2019-06-02 23:07:58'),
(10, 4, 'Centro de Computo', 'Router', 'Prueba', 'Formateo, ', 'F', '2019-06-02 23:09:05', '2019-06-02 23:09:05'),
(11, 5, 'Metalmecanica', 'Computadora', 'Prueba', 'Formateo, ', 'f', '2019-06-02 23:09:12', '2019-06-02 23:09:12'),
(12, 5, 'Metalmecanica', 'Computadora', 'Prueba', 'Formateo, ', 'f', '2019-06-02 23:13:09', '2019-06-02 23:13:09'),
(13, 2, 'Sistemas', 'Impresora', 'Prueba', 'Reparación, ', 'R', '2019-06-02 23:26:53', '2019-06-02 23:26:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`, `created_at`, `updated_at`) VALUES
(1, '2019_05_31_212908_create_accions_table', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mision`
--

CREATE TABLE `mision` (
  `id` smallint(6) NOT NULL,
  `mision` varchar(255) NOT NULL,
  `vision` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mision`
--

INSERT INTO `mision` (`id`, `mision`, `vision`) VALUES
(0, 'Formar profesionistas de excelencia en el ámbito de la ciencia y la tecnología, capaces de propiciar el desarrollo y transformación de su entorno, a través de programas educativos de calidad pertinentes..', 'Ser una institución educativa que se distinga como uno de los elementos fundamentales del desarrollo sustentable, permanente y equitativo del estado..');

-- --------------------------------------------------------

--
-- Table structure for table `router`
--

CREATE TABLE `router` (
  `id` smallint(6) NOT NULL,
  `Marca` varchar(20) NOT NULL,
  `Caracteristicas` varchar(50) NOT NULL,
  `ip_wan` varchar(50) NOT NULL,
  `ip_lan` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `router`
--

INSERT INTO `router` (`id`, `Marca`, `Caracteristicas`, `ip_wan`, `ip_lan`, `created_at`, `updated_at`) VALUES
(4, 'HP', 'Escritorio', '172.168.1.567', '172.168.1.567', '2019-05-26 22:46:27', '2019-05-26 22:46:27');

-- --------------------------------------------------------

--
-- Table structure for table `switches`
--

CREATE TABLE `switches` (
  `id` smallint(6) NOT NULL,
  `Marca` varchar(20) NOT NULL,
  `Caracteristicas` varchar(50) NOT NULL,
  `ip_admin` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `switches`
--

INSERT INTO `switches` (`id`, `Marca`, `Caracteristicas`, `ip_admin`, `created_at`, `updated_at`) VALUES
(1, 'HP', 'Escritorio', '192.168.1.1', '2019-05-22 18:52:01', '2019-05-22 18:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_cargo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `tipo_cargo`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'Prueba', '$2y$10$yqrGF18h2h8MFxQaIuL3tePRjaVuibtfx4MpKZ.CZlEYWHyUOGQGu', 'Oo4Wi1aS7lMUjfRd9IQEFyJM11UyySEOn5aGpZBFKEx8mGmkMyV7pWjuo4Vv', '2019-05-16 21:18:53', '2019-06-02 23:55:09'),
(3, 'Dirección', 'Director', '$2y$10$adR3ZM48unNuKTVgRq1uSOc5OTnpl.98zttkTuK.ruEuPE6CHFhSK', 'zS5Nf2Nb606OY461DosPbqRWIn6QKQc51uVM7RkTejLBWNJkxE7deQbf7gS0', '2019-06-03 04:09:35', '2019-06-03 04:09:35'),
(4, 'Servicio', 'Pedro1', '$2y$10$jiM5pbqmQHZG.SCa58kJIuEmbogkudOhytk6pDwqhQSvctjktoCfK', 'p9rszm7OZTwvMEWMZaRQxF2LMy6ayQZ08Tlqde3k1Y2vYH6S1IanIxspakWK', '2019-06-03 04:17:00', '2019-06-03 04:17:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accions`
--
ALTER TABLE `accions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `computadoras`
--
ALTER TABLE `computadoras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departamento_id` (`Departamento`);

--
-- Indexes for table `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `impresoras`
--
ALTER TABLE `impresoras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Departamento` (`Departamento`);

--
-- Indexes for table `mantenimiento`
--
ALTER TABLE `mantenimiento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mision`
--
ALTER TABLE `mision`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `router`
--
ALTER TABLE `router`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `switches`
--
ALTER TABLE `switches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accions`
--
ALTER TABLE `accions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `computadoras`
--
ALTER TABLE `computadoras`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` smallint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `impresoras`
--
ALTER TABLE `impresoras`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mantenimiento`
--
ALTER TABLE `mantenimiento`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `router`
--
ALTER TABLE `router`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `switches`
--
ALTER TABLE `switches`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `computadoras`
--
ALTER TABLE `computadoras`
  ADD CONSTRAINT `computadoras_ibfk_1` FOREIGN KEY (`Departamento`) REFERENCES `departamentos` (`id`);

--
-- Constraints for table `impresoras`
--
ALTER TABLE `impresoras`
  ADD CONSTRAINT `impresoras_ibfk_1` FOREIGN KEY (`Departamento`) REFERENCES `departamentos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
