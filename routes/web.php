<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/licence', function () {
    return view('licence');
});

Auth::routes();
Auth::routes([
  'verify' => false,
  'reset' => false
]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/usuarios', 'HomeController@users');
Route::post('/mision', 'HomeController@mision');
Route::get('/usuarios/{id}/edit', 'HomeController@editUser');
Route::PATCH('/usuarios/{id}', 'HomeController@updateUser');
Route::DELETE('/usuarios/{id}', 'HomeController@destroy');
Route::post('/rusuarios/{id}', 'HomeController@RestorePass');
Route::post('/rsusuarios/{id}', 'HomeController@ResetPass');
Route::get('/listu', 'HomeController@table');
Route::resource('computadoras', 'ComputadoraController');
Route::resource('impresoras', 'ImpresoraController');
Route::resource('routers', 'RouterController');
Route::resource('switches', 'SwitchController');
Route::resource('departamentos', 'DepartamentosController');
Route::resource('mantenimientos', 'MantenimientoController');
Route::resource('acciones', 'AccionesController');
Route::get('/lista', 'AccionesController@table');
Route::get('/listc', 'ComputadoraController@table');
Route::get('/listd', 'DepartamentosController@table');
Route::get('/listi', 'ImpresoraController@table');
Route::get('/listr', 'RouterController@table');
Route::get('/lists', 'SwitchController@table');
Route::get('/listm', 'MantenimientoController@table');
Route::get('/listcd', 'ComputadoraController@show');
Route::get('/listid', 'ImpresoraController@show');
Route::get('/listman', 'MantenimientoController@listR');
Route::get('/reportes', 'MantenimientoController@reportes');
Route::post('/cp', 'HomeController@CP');
